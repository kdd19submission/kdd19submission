# Experiments code for the kdd19 submission 

# Streaming quantiles algorithms with small space and update time

## Setup
To run the experiments: 
	1) unzip streams.zip file into streams folder, 
	   s.t. all the csv files have a path as "streams/abc.csv" 
           the name of each file with a stream starts from number x
	   which implies the legnth of the stream is 10^x
	2) make sure numpy package is installed 

Code for implementation of KLL and all its modifications is provided in kll.py 
Code for implementation of LWYC is provided in lwyc.py 


## Experiment 1: 
Compares all modes of KLL with LWYC on all streams available. 
To run the experiment 1:
```bash
	python eval.py -a exp1  -l 4 
```
-l specifies the length of the stream "-l 6" will run the experiment on the stream of length 10^6. 
Note, that in current repo only streams of length 10^4, 10^5 and 10^6 are provided	
For help on command line interface: 
```bash
python eval.py --help 
```

## Experiment 2: 
Compares vanilla KLL for the range of different parameters c. 
To run the experiment 2:
```bash
	python eval.py -a exp2   
```

## Experiment 3: 
Checks performance of LWYC and most promising 
configurations of KLL on different stream lengths.

To run the experiment 3:
```bash
	python eval.py -a exp3   
```

## Experiment 4: 
Checks performance of LWYC and most promising 
configurations of KLL on trending data with different ammount of trend. 

To run the experiment 4:
```bash
	python eval.py -a exp4   
```
